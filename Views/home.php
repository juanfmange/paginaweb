<!DOCTYPE html>
<html class="no-js">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Logistica Transmodal</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Transmodal se mueve a donde tu lo necesites. Contáctanos. Nos enfocamos en la seguridad de tu carga. Personal especializado en seguridad. Que asegura su carga desde el punto de partida hasta llegar al destino. Monitoreo por medio de drones. En TRANSMODAL utilizamos drones equipados para monitorear su carga." />
  <link rel="icon" href="<?= media(); ?>/images/transmodal.ico" type="image/ico" />
  <meta name="keywords" content="logistica" />
  <meta name="author" content="Gustavo R" />


  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
  <link rel="shortcut icon" href="favicon.ico">

  <link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/bootsnav.css">

  <!-- Animate.css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/animate.css">
  <!-- Icomoon Icon Fonts-->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/icomoon.css">
  <!-- Bootstrap  -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/bootstrap.css">
  <!-- Flexslider  -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/flexslider.css">
  <!-- Theme style  -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/style.css">
  <!-- Carrusel -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/owlcarousel/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/owlcarousel/owl.theme.default.min.css">
  <!-- Modernizr JS -->
  <script src=" <?php echo base_url(); ?>/Assets/js/modernizr-2.6.2.min.js">
  </script>
</head>

<body>
  <!-- Header -->
  <div id="fh5co-page">
    <header id="fh5co-header" role="banner">
      <div class="container">
        <div class="header-inner">
          <nav role="navigation" class="navbar navbar-inverse navbar-fixed-top">
            <ul style="padding-left: 100px;">
              <li><a class="" href="<?php echo base_url(); ?>">
                  <img src="<?= media(); ?>/images/logoAzul.png" alt="Image, Experiencias." style="width: 300px">
                </a></li>
              <li><a href="<?php echo base_url(); ?>/services">Servicios</a></li>
              <li><a href="<?php echo base_url(); ?>/naval">Naval</a></li>
              <li><a href="<?php echo base_url(); ?>/nosotros">Sobre nosotros</a></li>
              <li><a href="https://www.linkedin.com/company/transmodal-s-c/posts/?feedView=all" target="_BLANK">Noticias</a></li>
              <li><a href="<?php echo base_url(); ?>/sust">Sustentabilidad</a></li>
              <li><a href="https://blueservicessc-sandbox.com/Transmodal/" target="_BLANK">Clientes</a></li>
              <li><a class="btn btn-primary btn-outline" href="<?php echo base_url(); ?>/contacto">Contacto</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
    <!-- Inicio -->
    <aside id="fh5co-hero" class="js-fullheight">
      <div class="flexslider js-fullheight">
        <ul class="slides">
          <li style=" background-image: url(<?= media(); ?>/images/ima/_VM11754.jpg);">
            <div class="overlay-gradient"></div>
            <div class="container">
              <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                <div class="slider-text-inner">
                  <h2 align="left" style="color: #0E317A;">En 2025,<br>TRANSFORMAREMOS<br> LA LOGÍSTICA EN MÉXICO</h2>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </aside>
    <!--Enfoque en la seguridad -->
    <div id="fh5co-grid-products" class="animate-box" style="padding-top: 50px;">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
            <p align="justify"><b><i>Somos una empresa logística, con más de 20 años de historia, formando parte de un grupo consolidado, que conoce la forma de operar en México y siente como propios los intereses del cliente anteponiendo la PROTECCIÓN como elemento diferencial en sus operaciones</i></b></p>
          </div>
        </div>
      </div>
    </div>

    <div id="fh5co-why-us" class="animate-box">
      <section id="fh5co-trusted" data-section="trusted">
        <div class="fh5co-trusted">
          <div class="container">
            <div class="row">
              <div class="col-md-12 section-heading text-center">
                <h2 class="to-animate"></h2>
                <div class="row">
                  <div class="col-md-4 text-center item-block">
                    <span class="icon"><img src="<?= media(); ?>/images/candado.ico" alt="Imagen" class="img-responsive"></span>
                    <h3>Personal especializado en seguridad</h3>
                    <p align="justify">Que asegura su carga desde el punto de partida hasta llegar al destino. </p>
                  </div>
                  <div class="col-md-4 text-center item-block">
                    <span class="icon"><img src="<?= media(); ?>/images/icodron.ico" alt="Imagen" class="img-responsive"></span>
                    <h3>Monitoreo por medio de drones</h3>
                    <p align="justify">En TRANSMODAL utilizamos drones equipados para monitorear su carga. </p>
                  </div>
                  <div class="col-md-4 text-center item-block">
                    <span class="icon"><img src="<?= media(); ?>/images/camara.ico" alt="Imagen" class="img-responsive"></span>
                    <h3>Centro de monitoreo especializado</h3>
                    <p align="justify">Que tiene visibilidad y trazabilidad en todo momento dedicado a proteger la integridad de su carga. </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

    <!-- Flexbox de servicios -->
    <div id="fh5co-grid-products" class="animate-box" style="padding-top: 50px;">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
          </div>
        </div>
      </div>
      <div class="col-1">
        <a href="<?php echo base_url(); ?>/services" class="item-grid one" style="background-image: url(<?= media(); ?>/images/ima/_VM11728.jpg)">
          <div class="v-align">
            <div class="v-align-middle">
              <span class="icon"><img src="<?= media(); ?>/images/camion.ico" alt="Free HTML5 Templates" class="img-responsive"></span>
              <h3 class="title">Camión</h3>
              <h5 class="category">Control con TMS Transportation Management System</h5>
            </div>
          </div>
        </a>
        <a href="<?php echo base_url(); ?>/services" class="item-grid three" style="background-image: url(<?= media(); ?>/images/ima/_VM11737.jpg)">
          <div class="v-align">
            <div class="v-align-middle">
              <span class="icon"><img src="<?= media(); ?>/images/trainT.svg" alt="Free HTML5 Templates" class="img-responsive"></span>
              <h3 class="title">Ferrocarril</h3>
              <h5 class="category">Aprovecha todas las ventajas de mover tu carga por ferrocarril, incluso en el interior de México.</h5>
            </div>
          </div>
        </a>
      </div>
      <div class="col-2">
        <a href="<?php echo base_url(); ?>/services" class="item-grid two" style="background-image: url(<?= media(); ?>/images/ima/_VM11739.jpg)">
          <div class="v-align">
            <div class="v-align-middle">
              <span class="icon"><img src="<?= media(); ?>/images/auto-carga.svg" alt="Free HTML5 Templates" class="img-responsive"></span>
              <h3 class="title">Servicios personalizados</h3>
              <h5 class="category">Contrata un servicio personalizado en cuestión de minutos</h5>
            </div>
          </div>
        </a>
      </div>
    </div>

    <!-- Noticias y ultimas novedades -->
    <div id="fh5co-blog" class="animate-box">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
            <h2>Últimas novedades</h2>
            <p> </p>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <a class="fh5co-entry" href="<?php echo base_url(); ?>/contacto">
              <!--<a class="fh5co-entry" href="#"> -->
              <figure>
                <img src="<?= media(); ?>/images/ima/supv.jpg" alt="Image, Enfocados." class="img-responsive">
              </figure>
              <div class="fh5co-copy">
                <h3>Enfocados en el cliente</h3>
                <!--<span class="fh5co-date">Mayo 15, 2021</span>-->
                <p align=""> Nuestra filosofía y operación está centrada en el cliente, asegurándonos de que su experiencia sea inigualable. </p>
              </div>
          </div>
          <div class="col-md-4">
            <a class="fh5co-entry" href="#">
              <figure>
                <img src="<?= media(); ?>/images/ima/DSC_0017.jpg" alt="Image, Dinamismo." class="img-responsive">
              </figure>
              <div class="fh5co-copy">
                <h3>Seguridad</h3>
                <p align="justify">El pilar más importante de nuestra logística es la protección. Por eso tenemos un equipo especializado que se encarga de tu mercancía en todo momento.</p>
              </div>
          </div>
          <div class="col-md-4">
            <a class="fh5co-entry">
              <figure>
                <img src="<?= media(); ?>/images/ima/MBG9700.jpg" alt="Imagen, Ayudas." class="img-responsive">
              </figure>
              <div class="fh5co-copy">
                <h3>Compromiso</h3>
                <p align="justify">Damos nuestro mejor esfuerzo para satisfacer el nivel de servicio de nuestros clientes y generar un vínculo respetuoso y colaborativo con nuestros proveedores.</p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>

    <!-- Carrusel Empresas -->
    <div id="fh5co-why-us" class="animate-box">
      <section id="fh5co-trusted" data-section="trusted">
        <div class="fh5co-trusted">
          <div class="container">
            <div class="row">
              <div class="col-md-12 section-heading text-center">
                <!--<h2 class="to-animate">_____________________</h2>-->
                <div class="row">
                  <div class="col-md-8 col-md-offset-2 subtext">
                    <h3 class="to-animate"> Nuestros principales clientes </h3>
                  </div>
                </div>
              </div>
            </div>

            <div class="owl-container">
              <div class="owl-carousel owl-theme">
                <div class="item2"><img src="<?= media(); ?>/images/empresas/bras2.png" alt="Partners" class="img-responsive"></div>
                <div class="item2"><img src="<?= media(); ?>/images/empresas/diageo2.png" alt="Partners" class="img-responsive"></div>
                <div class="item2"><img src="<?= media(); ?>/images/empresas/pernod.png" alt="Partners" class="img-responsive"></div>
                <div class="item2"><img src="<?= media(); ?>/images/empresas/bln.png" alt="Partners" class="img-responsive"></div>
                <div class="item2"><img src="<?= media(); ?>/images/empresas/cosal.png" alt="Partners" class="img-responsive"></div>
                <div class="item2"><img src="<?= media(); ?>/images/empresas/casapedro2.png" alt="Partners" class="img-responsive"></div>
                <div class="item2"><img src="<?= media(); ?>/images/empresas/p&g2.png" alt="Partners" class="img-responsive"></div>
                <div class="item2"><img src="<?= media(); ?>/images/empresas/mattel2.png" alt="Partners" class="img-responsive"></div>
              </div>
            </div>

          </div>
        </div>
    </div>
    </section>

    <!-- Carrusel Certificaciones -->
    <div id="fh5co-why-us" class="animate-box">
      <section id="fh5co-trusted" data-section="trusted">
        <div class="fh5co-trusted">
          <div class="container">
            <div class="row">
              <div class="col-md-12 section-heading text-center">
                <!--<h2 class="to-animate">_____________________</h2>-->
                <div class="row">
                  <div class="col-md-8 col-md-offset-2 subtext">
                    <h3 class="to-animate"> Nuestras certificaciónes </h3>
                  </div>
                </div>
              </div>
            </div>

            <div class="owl-container">
              <div class="owl-carousel owl-theme">

                <div class="item2"><img src="<?= media(); ?>/images/certi/amtii.svg" alt="Partners" class="img-responsive"></div>
                <div class="item2"><img src="<?= media(); ?>/images/certi/amacarga.png" alt="Partners" class="img-responsive"></div>
                <div class="item2"><img src="<?= media(); ?>/images/certi/asis.png" alt="Partners" class="img-responsive"></div>
                <div class="item2"><img src="<?= media(); ?>/images/certi/Sello-CTPAT.png" alt="Partners" class="img-responsive"></div>
                <div class="item2"><img src="<?= media(); ?>/images/certi/fiata.png" alt="Partners" class="img-responsive"></div>
                <div class="item2"><img src="<?= media(); ?>/images/certi/wca-s.svg" alt="Partners" class="img-responsive"></div>
              </div>
            </div>

          </div>
        </div>
    </div>
    </section>

    <!-- Registo de servicios (prefooter) -->
    <div class="fh5co-cta" style="background-image: url(<?= media(); ?>/images/ima/MBG9666.jpg);">
      <div class="overlay"></div>
      <div class="container">
        <div class="col-md-12 text-center animate-box">
          <h3>Registro de servicios</h3>
          <p><a href="<?php echo base_url(); ?>/contacto" class="btn btn-primary btn-outline with-arrow">Contacto <i class="icon-arrow-right"></i></a></p>
        </div>
      </div>
    </div>
    <!-- footer -->
    <footer id="fh5co-footer" role="contentinfo">
      <!-- Nuestros servicios -->
      <div class="container">
        <div class="col-md-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
          <h3>Nuestros Servicios</h3>
          <ul class="float">
            <!--  <li><a>Seguridad &amp; Visualizacion</a></li>-->
            <li><a href="<?php echo base_url(); ?>/contacto">Marítimo</a></li>
            <li><a href="<?php echo base_url(); ?>/contacto">Ferrocarril</a></li>
            <li><a href="<?php echo base_url(); ?>/contacto">Camión</a></li>
            <li><a href="<?php echo base_url(); ?>/contacto">Almacén</a></li>
            <li><a href="<?php echo base_url(); ?>/contacto">Seguros</a></li>
            <li><a href="<?php echo base_url(); ?>/contacto">Servicio Personalizado</a></li>
            <li><a href="https://speechnotes.co/es/" target="_BLANK">Convertidor de audio</a></li>
            <li><a href="https://blueservicessc-sandbox.com/Transmodal/" target="_BLANK">Acceso a clientes</a></li>
            <li><a href="https://transmodal.com.mx/aviso-de-privacidad/" target="_BLANK">Aviso de Privacidad</a></li>
          </ul>
        </div>

        <!-- Siguenos -->
        <div class="col-md-4 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0 text-center">
          <h3>Síguenos</h3>
          <ul class="fh5co-social">
            <li><a href="https://twitter.com/TransmodalMx" target="_BLANK"><i class="icon-twitter"></i></a></li>
            <li><a></a></li>
            <li><a href="https://www.facebook.com/Transmodal.sc" target="_BLANK"><i class="icon-facebook"></i></a></li>
            <li><a></a></li>
            <li><a href="https://www.instagram.com/Transmodal_/" target="_BLANK"><i class="icon-instagram"></i></a></li>
            <li><a></a></li>
            <li><a href="https://www.linkedin.com/company/transmodal-s-c/mycompany/" target="_BLANK"><i class="icon-linkedin"></i></a></li>
          </ul>
        </div>

        <!-- Navegación -->
        <div class="col-md-2 col-md-push-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
          <h3>Navegación</h3>
          <ul>
            <li><a href="<?php echo base_url(); ?>/services">Servicios</a></li>
            <li><a href="#">Naval</a></li>
            <li><a href="<?php echo base_url(); ?>/nosotros">Sobre nosotros</a></li>
            <li><a href="https://www.linkedin.com/company/transmodal-s-c/posts/?feedView=all" target="_BLANK">Noticias</a></li>
            <li><a href="#">Sustentabilidad</a></li>
            <li><a href="https://blueservicessc-sandbox.com/Transmodal/" target="_BLANK">Acceso clientes</a></li>
            <li><a class="btn btn-primary btn-outline" href="<?php echo base_url(); ?>/contacto">Contacto</a></li>
          </ul>
        </div>


        <!-- Derechos de autor -->
        <div class="col-md-12 fh5co-copyright text-center">
          <p>&copy; 2021 Transmodal S.C. All Rights Reserved. <span>Designed by Transmodal TI 2021</span></p>
        </div>


      </div>
  </div>
  </div>
  </footer>
  <!-- Scripts Js -->
  <!-- jQuery -->
  <script src="<?= media(); ?>/js/jquery.min.js"></script>
  <!-- jQuery Easing -->
  <script src="<?= media(); ?>/js/jquery.easing.1.3.js"></script>
  <!-- Bootstrap -->
  <script src="<?= media(); ?>/js/bootstrap.min.js"></script>
  <!-- Waypoints -->
  <script src="<?= media(); ?>/js/jquery.waypoints.min.js"></script>
  <!-- Flexslider -->
  <script src="<?= media(); ?>/js/jquery.flexslider-min.js"></script>
  <!-- Carrusel 
    <script src="<?php echo base_url(); ?>/Assets/owlcarousel/jquery.min.js"></script>-->
  <script src="<?php echo base_url(); ?>/Assets/owlcarousel/owl.carousel.min.js"></script>
  <!-- MAIN JS -->
  <script src="<?= media(); ?>/js/main.js"></script>
  <script src="<?= media(); ?>/js/main2.js"></script>
</body>

</html>