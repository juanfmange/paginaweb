<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="Transmodal se mueve a donde tu lo necesites. Contáctanos. Nos enfocamos en la seguridad de tu carga. Personal especializado en seguridad. Que asegura su carga desde el punto de partida hasta llegar al destino. Monitoreo por medio de drones. En TRANSMODAL utilizamos drones equipados para monitorear su carga."> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Gustavo R">
    <meta name="theme-color" content="#0000FF">
    <link rel="icon" href="<?= media(); ?>/images/transmodal.ico" type="image/ico" />
    <title><?= $data['page_tag']  ?></title>
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="<?= media(); ?>/css/main.css">
    <link rel="stylesheet" type="text/css" href="<?= media(); ?>/css/style1.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="<?= base_url(); ?>/dashboard">Dashboard</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
       
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="<?= base_url(); ?>/opciones"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
            <li><a class="dropdown-item" href="<?= base_url(); ?>/perfil"><i class="fa fa-user fa-lg"></i> Profile</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url(); ?>"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>  <!-- el de verdaaadd href="<?= base_url(); ?>/logout"-->
          </ul>
        </li>
      </ul>
    </header>
    <?php require_once("nav_admin.php"); ?>