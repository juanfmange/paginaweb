<!DOCTYPE html>

<html class="no-js">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Logistica Transmodal</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Transmodal se mueve a donde tu lo necesites. Contáctanos. Nos enfocamos en la seguridad de tu carga. Personal especializado en seguridad. Que asegura su carga desde el punto de partida hasta llegar al destino. Monitoreo por medio de drones. En TRANSMODAL utilizamos drones equipados para monitorear su carga." />
	<link rel="icon" href="<?= media(); ?>/images/transmodal.ico" type="image/ico" />
	<meta name="keywords" content="" />
	<meta name="author" content="Gustavo R" />

	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="" />
	<meta property="og:image" content="" />
	<meta property="og:url" content="" />
	<meta property="og:site_name" content="" />
	<meta property="og:description" content="" />
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'> -->

	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/bootstrap.css">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/flexslider.css">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/style.css">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url(); ?>/Assets/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>
	<!-- Header -->
	<div id="fh5co-page">
		<header id="fh5co-header" role="banner">
			<div class="container">
				<div class="header-inner">
					<nav role="navigation" class="navbar navbar-inverse navbar-fixed-top">
						<ul style="padding-left: 100px;">
							<li><a class="" href="<?php echo base_url(); ?>">
									<img src="<?= media(); ?>/images/logoAzul.png" alt="Image, Experiencias." style="width: 300px">
								</a></li>
							<li><a href="<?php echo base_url(); ?>/services">Servicios</a></li>
							<li><a href="<?php echo base_url(); ?>/naval">Naval</a></li>
							<li><a href="<?php echo base_url(); ?>/nosotros">Sobre nosotros</a></li>
							<li><a href="https://www.linkedin.com/company/transmodal-s-c/posts/?feedView=all" target="_BLANK">Noticias</a></li>
							<li><a href="<?php echo base_url(); ?>/sust">Sustentabilidad</a></li>
							<li><a href="https://blueservicessc-sandbox.com/Transmodal/" target="_BLANK">Clientes</a></li>
							<li><a class="btn btn-primary btn-outline" href="<?php echo base_url(); ?>/contacto">Contacto</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>

		<aside id="fh5co-hero" clsas="js-fullheight">
			<div class="flexslider js-fullheight">
				<ul class="slides">
					<li style="background-image: url(<?= media(); ?>/images/ima/_VM11643.jpg);">
						<div class="overlay-gradient"></div>
						<div class="container">
							<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
								<div class="slider-text-inner">
									<h2 style="color: #0E317A;">¡Tu mercancía nuestra prioridad!</h2>
									<!--<p class="fh5co-lead">En Transmodal nos encargamos de que tu mercancía esté al alcance de todos sin importar donde estén.</p>-->
									<!--<p class="fh5co-lead">En Transmodal nos encargamos de que tu mercancía esté al alcance de todos sin importar donde estén <a href="#" target="_blank"> Texto con hipervinculo</a></p> -->
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</aside>


		<div class="fh5co-services">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
						<h2>Nuestro placer servirte</h2>
						<p align="justify">Si lo que buscas es que tu marca llegue a los mayores super mercados nosotros la podemos transportar, visualiza nuestros servicios para ver cuál es la mejor opción para ti. </p>
					</div>
					<div class="col-md-4 text-center item-block animate-box">
						<span class="icon"><img src="<?= media(); ?>/images/camion.ico" alt="Camión" class="img-responsive"></span>
						<h3>Camión</h3>
						<p align="justify">Control por TMS - Un sistema de Información que recoge, almacena, procesa y distribuye la información relacionada con las operaciones de transporte de mercancías.</p>
						<p><a href="<?php echo base_url(); ?>/contacto" class="btn btn-primary with-arrow">Cotiza <i class="icon-arrow-right"></i></a></p>
					</div>
					<div class="col-md-4 text-center item-block animate-box">
						<span class="icon"><img src="<?= media(); ?>/images/maritimo.ico" alt="Marítimo" class="img-responsive"></span>
						<h3>Marítimo</h3>
						<p align="justify">Movemos tu carga a cualquier puerto del mundo. Contamos con socios comerciales en diversas terminales porturarias. Alianzas estratégicas con líneas navieras más importantes del mundo</p>
						<p><a href="<?php echo base_url(); ?>/contacto" class="btn btn-primary with-arrow">Cotiza <i class="icon-arrow-right"></i></a></p>
					</div>
					<div class="col-md-4 text-center item-block animate-box">
						<span class="icon"><img src="<?= media(); ?>/images/tren.ico" alt="Ferrocarril" class="img-responsive"></span>
						<h3>Ferrocarril</h3>
						<p align="justify">Con Opciones tipo: CROSSBORDER, DOMÉSTICO e INDUSTRIAL. Ya que contamos con una amplia variedad de equipo especializado para mover todo tipo de carga.</p>
						<p><a href="<?php echo base_url(); ?>/contacto" class="btn btn-primary with-arrow">Cotiza <i class="icon-arrow-right"></i></a></p>
					</div>

					<div class="col-md-4 text-center item-block animate-box">
						<span class="icon"><img src="<?= media(); ?>/images/almace.ico" alt="Almacén" class="img-responsive"></span>
						<h3>Almacén</h3>
						<p align="justify">Servicio de almacenaje nacional y fiscal. Nos adaptamos a los requerimientos y necesidades del cliente. Implementamos una cadena de suministro más inteligente, mas amplia y menos costosa.</p>
						<p><a href="<?php echo base_url(); ?>/contacto" class="btn btn-primary with-arrow">Cotiza <i class="icon-arrow-right"></i></a></p>
					</div>
					<div class="col-md-4 text-center item-block animate-box">
						<span class="icon"><img src="<?= media(); ?>/images/seguridad2.ico" alt="Seguros" class="img-responsive"></span>
						<h3>Seguros</h3>
						<p align="justify">Seguros para Mercancía en tránsito en cualquier parte del mundo. Te asesoramos en las operaciones de movimiento de carga y te acompañamos durante todo el proceso en el caso de siniestro.</p>
						<p><a href="<?php echo base_url(); ?>/contacto" class="btn btn-primary with-arrow">Cotiza <i class="icon-arrow-right"></i></a></p>
					</div>
					<div class="col-md-4 text-center item-block animate-box">
						<span class="icon"><img src="<?= media(); ?>/images/personalizado.ico" alt="Servicio Personalizado" class="img-responsive"></span>
						<h3>Servicio Personalizado</h3>
						<p align="justify">Contamos con medidas para proteger tu mercancía en puntos específicos, sabemos lo que tu mercancía significa para ti, contrata un servicio personalizado en cuestión de minutos de acuerdo a lo que necesites, solo contáctanos. </p>
						<p><a href="<?php echo base_url(); ?>/contacto" class="btn btn-primary with-arrow">Cotiza <i class="icon-arrow-right"></i></a></p>
					</div>
				</div>
			</div>
		</div>

		<div class="fh5co-cta" style="background-image: url(<?= media(); ?>/images/ima/MBG9666.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="col-md-12 text-center animate-box">
					<h3>Contrata nuestros servicios que tenemos para ti. </h3>
					<p><a href="<?php echo base_url(); ?>/contacto" class="btn btn-primary btn-outline with-arrow">Contactar! <i class="icon-arrow-right"></i></a></p>
				</div>
			</div>
		</div>

		<!-- footer -->
		<footer id="fh5co-footer" role="contentinfo">
			<!-- Nuestros servicios -->
			<div class="container">
				<div class="col-md-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
					<h3>Nuestros Servicios</h3>
					<ul class="float">
						<!--  <li><a>Seguridad &amp; Visualizacion</a></li>-->
						<li><a href="<?php echo base_url(); ?>/contacto">Marítimo</a></li>
						<li><a href="<?php echo base_url(); ?>/contacto">Ferrocarril</a></li>
						<li><a href="<?php echo base_url(); ?>/contacto">Camión</a></li>
						<li><a href="<?php echo base_url(); ?>/contacto">Almacén</a></li>
						<li><a href="<?php echo base_url(); ?>/contacto">Seguros</a></li>
						<li><a href="<?php echo base_url(); ?>/contacto">Servicio Personalizado</a></li>
						<li><a href="https://speechnotes.co/es/" target="_BLANK">Convertidor de audio</a></li>
						<li><a href="https://blueservicessc-sandbox.com/Transmodal/" target="_BLANK">Acceso a clientes</a></li>
						<li><a href="https://transmodal.com.mx/aviso-de-privacidad/" target="_BLANK">Aviso de Privacidad</a></li>
					</ul>
				</div>

				<!-- Siguenos -->
				<div class="col-md-4 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0 text-center">
					<h3>Síguenos</h3>
					<ul class="fh5co-social">
						<li><a href="https://twitter.com/TransmodalMx" target="_BLANK"><i class="icon-twitter"></i></a></li>
						<li><a></a></li>
						<li><a href="https://www.facebook.com/Transmodal.sc" target="_BLANK"><i class="icon-facebook"></i></a></li>
						<li><a></a></li>
						<li><a href="https://www.instagram.com/Transmodal_/" target="_BLANK"><i class="icon-instagram"></i></a></li>
						<li><a></a></li>
						<li><a href="https://www.linkedin.com/company/transmodal-s-c/mycompany/" target="_BLANK"><i class="icon-linkedin"></i></a></li>
					</ul>
				</div>

				<!-- Navegación -->
				<div class="col-md-2 col-md-push-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
					<h3>Navegación</h3>
					<ul>
						<li><a href="<?php echo base_url(); ?>/services">Servicios</a></li>
						<li><a href="#">Naval</a></li>
						<li><a href="<?php echo base_url(); ?>/nosotros">Sobre nosotros</a></li>
						<li><a href="https://www.linkedin.com/company/transmodal-s-c/posts/?feedView=all" target="_BLANK">Noticias</a></li>
						<li><a href="#">Sustentabilidad</a></li>
						<li><a href="https://blueservicessc-sandbox.com/Transmodal/" target="_BLANK">Acceso clientes</a></li>
						<li><a class="btn btn-primary btn-outline" href="<?php echo base_url(); ?>/contacto">Contacto</a></li>
					</ul>
				</div>


				<!-- Derechos de autor -->
				<div class="col-md-12 fh5co-copyright text-center">
					<p>&copy; 2021 Transmodal S.C. All Rights Reserved. <span>Designed by Transmodal TI 2021</span></p>
				</div>


			</div>
	</div>
	</div>
	</footer>
	</div>



	<!-- jQuery -->
	<script src="<?= media(); ?>/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?= media(); ?>/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?= media(); ?>/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?= media(); ?>/js/jquery.waypoints.min.js"></script>
	<!-- Easy PieChart -->
	<script src="<?= media(); ?>/js/jquery.easypiechart.min.js"></script>
	<!-- Flexslider -->
	<script src="<?= media(); ?>/js/jquery.flexslider-min.js"></script>
	<!-- Stellar -->
	<script src="<?= media(); ?>/js/jquery.stellar.min.js"></script>

	<!-- MAIN JS -->
	<script src="<?= media(); ?>/js/main.js"></script>

</body>

</html>