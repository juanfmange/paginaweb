<!DOCTYPE html>

<html class="no-js">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Logistica Transmodal</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Transmodal se mueve a donde tu lo necesites. Contáctanos. Nos enfocamos en la seguridad de tu carga. Personal especializado en seguridad. Que asegura su carga desde el punto de partida hasta llegar al destino. Monitoreo por medio de drones. En TRANSMODAL utilizamos drones equipados para monitorear su carga." />
	<link rel="icon" href="<?= media(); ?>/images/transmodal.ico" type="image/ico" />
	<meta name="keywords" content="" />
	<meta name="author" content="Gustavo R" />

	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="" />
	<meta property="og:image" content="" />
	<meta property="og:url" content="" />
	<meta property="og:site_name" content="" />
	<meta property="og:description" content="" />
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'> -->

	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/bootstrap.css">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/flexslider.css">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/style.css">

	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/bootsnav.css">

	<!-- Carrusel -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/owlcarousel/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/owlcarousel/owl.theme.default.min.css">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url(); ?>/Assets/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>
	<!-- Header -->
	<div id="fh5co-page">
		<header id="fh5co-header" role="banner">
			<div class="container">
				<div class="header-inner">
					<nav role="navigation" class="navbar navbar-inverse navbar-fixed-top">
						<ul style="padding-left: 100px;">
							<li><a class="" href="<?php echo base_url(); ?>">
									<img src="<?= media(); ?>/images/logoAzul.png" alt="Image, Experiencias." style="width: 300px">
								</a></li>
							<li><a href="<?php echo base_url(); ?>/services">Servicios</a></li>
							<li><a href="<?php echo base_url(); ?>/naval">Naval</a></li>
							<li><a href="<?php echo base_url(); ?>/nosotros">Sobre nosotros</a></li>
							<li><a href="https://www.linkedin.com/company/transmodal-s-c/posts/?feedView=all" target="_BLANK">Noticias</a></li>
							<li><a href="<?php echo base_url(); ?>/sust">Sustentabilidad</a></li>
							<li><a href="https://blueservicessc-sandbox.com/Transmodal/" target="_BLANK">Clientes</a></li>
							<li><a class="btn btn-primary btn-outline" href="<?php echo base_url(); ?>/contacto">Contacto</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>
		<!-- Inicio de nosotros -->
		<aside id="fh5co-hero" clsas="js-fullheight">
			<div class="flexslider js-fullheight">
				<ul class="slides">
					<li style="background-image: url(<?= media(); ?>/images/ima/_VM11725.jpg);">
						<div class="overlay-gradient"></div>
						<div class="container">
							<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
								<div class="slider-text-inner">
									<h2 align="right" style="color: #0E317A;">PROTEGE<br>TU PLANETA</h2>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</aside>

		<div class="fh5co-box animate-box">
			<img src=" <?= media(); ?>/images/sust/WebSostenibilidad.png" alt="sust" class="img-responsive">
		</div>
	</div>
	<!-- Prefooter -->
	<div class="fh5co-cta" style="background-image: url(<?= media(); ?>/images/ima/MBG9666.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="col-md-12 text-center animate-box">
				<h3>Registro de servicios</h3>
				<p><a href="<?php echo base_url(); ?>/contacto" class="btn btn-primary btn-outline with-arrow">Comienza ahora! <i class="icon-arrow-right"></i></a></p>
			</div>
		</div>
	</div>

	<!-- footer -->
	<footer id="fh5co-footer" role="contentinfo">
		<!-- Nuestros servicios -->
		<div class="container">
			<div class="col-md-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
				<h3>Nuestros Servicios</h3>
				<ul class="float">
					<!--  <li><a>Seguridad &amp; Visualizacion</a></li>-->
					<li><a href="<?php echo base_url(); ?>/contacto">Marítimo</a></li>
					<li><a href="<?php echo base_url(); ?>/contacto">Ferrocarril</a></li>
					<li><a href="<?php echo base_url(); ?>/contacto">Camión</a></li>
					<li><a href="<?php echo base_url(); ?>/contacto">Almacén</a></li>
					<li><a href="<?php echo base_url(); ?>/contacto">Seguros</a></li>
					<li><a href="<?php echo base_url(); ?>/contacto">Servicio Personalizado</a></li>
					<li><a href="https://speechnotes.co/es/" target="_BLANK">Convertidor de audio</a></li>
					<li><a href="https://blueservicessc-sandbox.com/Transmodal/" target="_BLANK">Acceso a clientes</a></li>
					<li><a href="https://transmodal.com.mx/aviso-de-privacidad/" target="_BLANK">Aviso de Privacidad</a></li>
				</ul>
			</div>

			<!-- Siguenos -->
			<div class="col-md-4 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0 text-center">
				<h3>Síguenos</h3>
				<ul class="fh5co-social">
					<li><a href="https://twitter.com/TransmodalMx" target="_BLANK"><i class="icon-twitter"></i></a></li>
					<li><a></a></li>
					<li><a href="https://www.facebook.com/Transmodal.sc" target="_BLANK"><i class="icon-facebook"></i></a></li>
					<li><a></a></li>
					<li><a href="https://www.instagram.com/Transmodal_/" target="_BLANK"><i class="icon-instagram"></i></a></li>
					<li><a></a></li>
					<li><a href="https://www.linkedin.com/company/transmodal-s-c/mycompany/" target="_BLANK"><i class="icon-linkedin"></i></a></li>
				</ul>
			</div>
			<!-- Navegación -->
			<div class="col-md-2 col-md-push-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
				<h3>Navegación</h3>
				<ul>
					<li><a href="<?php echo base_url(); ?>/services">Servicios</a></li>
					<li><a href="#">Naval</a></li>
					<li><a href="<?php echo base_url(); ?>/nosotros">Sobre nosotros</a></li>
					<li><a href="https://www.linkedin.com/company/transmodal-s-c/posts/?feedView=all" target="_BLANK">Noticias</a></li>
					<li><a href="#">Sustentabilidad</a></li>
					<li><a href="https://blueservicessc-sandbox.com/Transmodal/" target="_BLANK">Acceso clientes</a></li>
					<li><a class="btn btn-primary btn-outline" href="<?php echo base_url(); ?>/contacto">Contacto</a></li>
				</ul>
			</div>


			<!-- Derechos de autor -->
			<div class="col-md-12 fh5co-copyright text-center">
				<p>&copy; 2021 Transmodal S.C. All Rights Reserved. <span>Designed by Transmodal TI 2021</span></p>
			</div>


		</div>
		</div>
		</div>
	</footer>
	</div>


	<!-- jQuery -->
	<script src="<?= media(); ?>/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?= media(); ?>/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?= media(); ?>/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?= media(); ?>/js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="<?= media(); ?>/js/jquery.flexslider-min.js"></script>

	<!-- Carrusel 
    <script src="<?php echo base_url(); ?>/Assets/owlcarousel/jquery.min.js"></script>-->
	<script src="<?php echo base_url(); ?>/Assets/owlcarousel/owl.carousel.min.js"></script>

	<!-- MAIN JS -->
	<script src="<?= media(); ?>/js/main.js"></script>
	<script src="<?= media(); ?>/js/main2.js"></script>

</body>

</html>