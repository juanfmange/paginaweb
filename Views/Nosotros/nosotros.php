<!DOCTYPE html>

<html class="no-js">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Logistica Transmodal</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Transmodal se mueve a donde tu lo necesites. Contáctanos. Nos enfocamos en la seguridad de tu carga. Personal especializado en seguridad. Que asegura su carga desde el punto de partida hasta llegar al destino. Monitoreo por medio de drones. En TRANSMODAL utilizamos drones equipados para monitorear su carga." />
	<link rel="icon" href="<?= media(); ?>/images/transmodal.ico" type="image/ico" />
	<meta name="keywords" content="" />
	<meta name="author" content="Gustavo R" />

	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="" />
	<meta property="og:image" content="" />
	<meta property="og:url" content="" />
	<meta property="og:site_name" content="" />
	<meta property="og:description" content="" />
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'> -->

	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/bootstrap.css">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/flexslider.css">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/style.css">

	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/bootsnav.css">

	<!-- Carrusel -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/owlcarousel/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/owlcarousel/owl.theme.default.min.css">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url(); ?>/Assets/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>
	<!-- Header -->
	<div id="fh5co-page">
		<header id="fh5co-header" role="banner">
			<div class="container">
				<div class="header-inner">
					<nav role="navigation" class="navbar navbar-inverse navbar-fixed-top">
						<ul style="padding-left: 100px;">
							<li><a class="" href="<?php echo base_url(); ?>">
									<img src="<?= media(); ?>/images/logoAzul.png" alt="Image, Experiencias." style="width: 300px">
								</a></li>
							<li><a href="<?php echo base_url(); ?>/services">Servicios</a></li>
							<li><a href="<?php echo base_url(); ?>/naval">Naval</a></li>
							<li><a href="<?php echo base_url(); ?>/nosotros">Sobre nosotros</a></li>
							<li><a href="https://www.linkedin.com/company/transmodal-s-c/posts/?feedView=all" target="_BLANK">Noticias</a></li>
							<li><a href="<?php echo base_url(); ?>/sust">Sustentabilidad</a></li>
							<li><a href="https://blueservicessc-sandbox.com/Transmodal/" target="_BLANK">Clientes</a></li>
							<li><a class="btn btn-primary btn-outline" href="<?php echo base_url(); ?>/contacto">Contacto</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>
		<!-- Inicio de nosotros -->
		<aside id="fh5co-hero" clsas="js-fullheight">
			<div class="flexslider js-fullheight">
				<ul class="slides">
					<li style="background-image: url(<?= media(); ?>/images/ima/_MBG9323.jpg);">
						<div class="overlay-gradient"></div>
						<div class="container">
							<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
								<div class="slider-text-inner">
									<h2 align="right" style="color: #0E317A;padding-bottom: 270px;">SOMOS APASIONADOS <br> POR NUESTRO TRABAJO</h2>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</aside>

		<h2></h2>
		<h3 align="center">Nuestro Trayecto</h3>
		<div class="owl-container2">
			<div class="owl-carousel owl-theme">
				<div class="item2"><img src="<?= media(); ?>/images/historia/Web2001.png" alt="Partners" class="img-responsive"></div>
				<div class="item2"><img src="<?= media(); ?>/images/historia/Web2009.png" alt="Partners" class="img-responsive"></div>
				<div class="item2"><img src="<?= media(); ?>/images/historia/Web2012.png" alt="Partners" class="img-responsive"></div>
				<div class="item2"><img src="<?= media(); ?>/images/historia/Web2013.png" alt="Partners" class="img-responsive"></div>
				<div class="item2"><img src="<?= media(); ?>/images/historia/Web2016.png" alt="Partners" class="img-responsive"></div>
				<div class="item2"><img src="<?= media(); ?>/images/historia/Web2017.png" alt="Partners" class="img-responsive"></div>
			</div>
			<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
				<p align="justify">Somos una empresa 100% mexicana de transportación logística nacional e internacional.
					Fundada en el año 2001, Transmodal S.C. nace de una visión enfocada en brindar y desarrollar la transportación ferroviaria siendo esta la estrategia principal para un nuevo enfoque logístico. </p>
			</div>
		</div>


		<div class="fh5co-about animate-box">

			<div class="container">
				<div class="col-md-12">


					<div class="col-md-8 col-md-push-2">

						<h2 align="center">Nuestra Historia</h2>


						<h2></h2>

						<p align="justify">Contamos con oficinas en los principales puertos de México, dos sucursales en CDMX y coordinación logística en la terminal intermodal de TILH. A nivel internacional, contamos con oficina en Shanghai, China, uno de los principales orígenes comerciales para la importación y exportación.</p>
						<h2></h2>

						<!--<p><a href="#" class="btn btn-primary btn-outline with-arrow">Leer Más <i class="icon-arrow-right"></i></a></p> -->

						<figure>
							<img src="<?= media(); ?>/images/historia/mision1.png" alt="Misión." class="img-responsive">
						</figure>
						<figure>
							<img src="<?= media(); ?>/images/historia/vision1.png" alt="Visión." class="img-responsive">
						</figure>
					</div>
				</div>
			</div>
		</div>


		<!-- Slide proteje lo tuyo 
		<div id="fh5co-why-us" class="animate-box">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
						<h2>Estrategia de seguridad </h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/nosotros/centrocontrol.png" alt="Imagen" class="img-responsive"></span>
						<h3>Centro de<br> control 24/7</h3>
					</div>
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/nosotros/operativonaval.png" alt="Imagen" class="img-responsive"></span>
						<h3>Operativa <br>naval</h3>
					</div>
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/nosotros/monitoreodron.png" alt="Imagen" class="img-responsive"></span>
						<h3>Centro de<br> monitoreo <br>especializado</h3>
					</div>
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/nosotros/rutas.png" alt="Imagen" class="img-responsive"></span>
						<h3>Rutas <br>seguras </h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/nosotros/acreditaciones.png" alt="Imagen" class="img-responsive"></span>
						<h3>Acreditaciones</h3>
					</div>
					<div class="col-md-4 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/nosotros/personal.png" alt="Imagen" class="img-responsive"></span>
						<h3>Personal<br> capacitado</h3>
					</div>
					<div class="col-md-4 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/nosotros/alertas.png" alt="Imagen" class="img-responsive"></span>
						<h3>Alertas y TIC</h3>
					</div>
				</div>
			</div>
		</div>

	 Principios 
		<div id="fh5co-why-us" class="animate-box">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
						<h2>Principios de Transmodal </h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/principios/viaje.png" alt="Imagen" class="img-responsive"></span>
						<h3>Protege tu viaje</h3>
						<p align="justify center">tus productos</p>
					</div>
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/principios/valor.png" alt="Imagen" class="img-responsive"></span>
						<h3>Protege tu valor</h3>
						<p align="justify center">tus finanzas</p>
					</div>
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/principios/calidad.png" alt="Imagen" class="img-responsive"></span>
						<h3>Protege tu calidad</h3>
						<p align="justify center">tus indicadores</p>
					</div>
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/principios/imagen.png" alt="Imagen" class="img-responsive"></span>
						<h3>Protege tu imagen</h3>
						<p align="justify center">tu marketing</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/principios/amigos.png" alt="Imagen" class="img-responsive"></span>
						<h3>Protege tus amigos</h3>
						<p align="justify center">tus socios/partners</p>
					</div>
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/principios/fans.png" alt="Imagen" class="img-responsive"></span>
						<h3>Protege tus fans</h3>
						<p align="justify center">tus clientes</p>
					</div>
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/principios/personas.png" alt="Imagen" class="img-responsive"></span>
						<h3>Protege las personas</h3>
						<p align="justify center">tus trabajadores</p>
					</div>
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/principios/ma.png" alt="Imagen" class="img-responsive"></span>
						<h3>Protege el MA</h3>
						<p align="justify center">tu planeta</p>

					</div>
				</div>
			</div>
		</div>

		Valores 
		<div id="fh5co-why-us" class="animate-box">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
						<h2>Valores </h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/valores/cliente.png" alt="Imagen" class="img-responsive"></span>
						<h3>Clientes<br> únicos</h3>
					</div>
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/valores/soluciones.png" alt="Imagen" class="img-responsive"></span>
						<h3>Soluciones <br>3PL y 4PL</h3>
					</div>
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/valores/aseguramos.png" alt="Imagen" class="img-responsive"></span>
						<h3>Aseguramos<br>tu logística</h3>
					</div>
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/valores/financiamos.png" alt="Imagen" class="img-responsive"></span>
						<h3>Financiamos <br>tu servicio </h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/valores/kpi.png" alt="Imagen" class="img-responsive"></span>
						<h3>KPIs de calidad <br>y desarrollo </h3>
					</div>
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/valores/atencion.jpg" alt="Imagen" class="img-responsive"></span>
						<h3>Atención 24/7</h3>
					</div>
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/valores/grupo.jpg" alt="Imagen" class="img-responsive"></span>
						<h3>Grupo empresa<br>+ 100 personas</h3>
					</div>
					<div class="col-md-3 text-center item-block">
						<span class="icon"><img src="<?= media(); ?>/images/valores/anos.png" alt="Imagen" class="img-responsive"></span>
						<h3>Know-How<br>+ 20 años historia</h3>
					</div>
				</div>
			</div>
		</div>
-->



		<div class="fh5co-cta" style="background-image: url(<?= media(); ?>/images/ima/MBG9666.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="col-md-12 text-center animate-box">
					<h3>Registro de servicios</h3>
					<p><a href="<?php echo base_url(); ?>/contacto" class="btn btn-primary btn-outline with-arrow">Comienza ahora! <i class="icon-arrow-right"></i></a></p>
				</div>
			</div>
		</div>

		<!-- footer -->
		<footer id="fh5co-footer" role="contentinfo">
			<!-- Nuestros servicios -->
			<div class="container">
				<div class="col-md-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
					<h3>Nuestros Servicios</h3>
					<ul class="float">
						<!--  <li><a>Seguridad &amp; Visualizacion</a></li>-->
						<li><a href="<?php echo base_url(); ?>/contacto">Marítimo</a></li>
						<li><a href="<?php echo base_url(); ?>/contacto">Ferrocarril</a></li>
						<li><a href="<?php echo base_url(); ?>/contacto">Camión</a></li>
						<li><a href="<?php echo base_url(); ?>/contacto">Almacén</a></li>
						<li><a href="<?php echo base_url(); ?>/contacto">Seguros</a></li>
						<li><a href="<?php echo base_url(); ?>/contacto">Servicio Personalizado</a></li>
						<li><a href="https://speechnotes.co/es/" target="_BLANK">Convertidor de audio</a></li>
						<li><a href="https://blueservicessc-sandbox.com/Transmodal/" target="_BLANK">Acceso a clientes</a></li>
						<li><a href="https://transmodal.com.mx/aviso-de-privacidad/" target="_BLANK">Aviso de Privacidad</a></li>
					</ul>
				</div>

				<!-- Siguenos -->
				<div class="col-md-4 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0 text-center">
					<h3>Síguenos</h3>
					<ul class="fh5co-social">
						<li><a href="https://twitter.com/TransmodalMx" target="_BLANK"><i class="icon-twitter"></i></a></li>
						<li><a></a></li>
						<li><a href="https://www.facebook.com/Transmodal.sc" target="_BLANK"><i class="icon-facebook"></i></a></li>
						<li><a></a></li>
						<li><a href="https://www.instagram.com/Transmodal_/" target="_BLANK"><i class="icon-instagram"></i></a></li>
						<li><a></a></li>
						<li><a href="https://www.linkedin.com/company/transmodal-s-c/mycompany/" target="_BLANK"><i class="icon-linkedin"></i></a></li>
					</ul>
				</div>
				<!-- sobre nosotros -->
				<!--<div class="container">
          <div class="col-md-4 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
            <h3 align="center">Sobre Nosotros</h3>
            <p align="justify">Somos una empresa 100% mexicana de transportación logística nacional e internacional. Fundada en el año 2001, Transmodal S.C. nace de una visión enfocada en brindar y desarrollar la transportación ferroviaria siendo esta la estrategia principal para un nuevo enfoque logístico.</p>
          </div>-->

				<!-- Navegación -->
				<div class="col-md-2 col-md-push-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
					<h3>Navegación</h3>
					<ul>
						<li><a href="<?php echo base_url(); ?>/services">Servicios</a></li>
						<li><a href="#">Naval</a></li>
						<li><a href="<?php echo base_url(); ?>/nosotros">Sobre nosotros</a></li>
						<li><a href="https://www.linkedin.com/company/transmodal-s-c/posts/?feedView=all" target="_BLANK">Noticias</a></li>
						<li><a href="#">Sustentabilidad</a></li>
						<li><a href="https://blueservicessc-sandbox.com/Transmodal/" target="_BLANK">Acceso clientes</a></li>
						<li><a class="btn btn-primary btn-outline" href="<?php echo base_url(); ?>/contacto">Contacto</a></li>
					</ul>
				</div>


				<!-- Derechos de autor -->
				<div class="col-md-12 fh5co-copyright text-center">
					<p>&copy; 2021 Transmodal S.C. All Rights Reserved. <span>Designed by Transmodal TI 2021</span></p>
				</div>


			</div>
	</div>
	</div>
	</footer>
	</div>


	<!-- jQuery -->
	<script src="<?= media(); ?>/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?= media(); ?>/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?= media(); ?>/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?= media(); ?>/js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="<?= media(); ?>/js/jquery.flexslider-min.js"></script>

	<!-- Carrusel 
    <script src="<?php echo base_url(); ?>/Assets/owlcarousel/jquery.min.js"></script>-->
	<script src="<?php echo base_url(); ?>/Assets/owlcarousel/owl.carousel.min.js"></script>

	<!-- MAIN JS -->
	<script src="<?= media(); ?>/js/main.js"></script>
	<script src="<?= media(); ?>/js/main2.js"></script>

</body>

</html>