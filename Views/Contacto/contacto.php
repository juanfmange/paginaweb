<!DOCTYPE html>
<html lang="es">
<html class="no-js">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Logistica Transmodal</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Transmodal se mueve a donde tu lo necesites. Contáctanos. Nos enfocamos en la seguridad de tu carga. Personal especializado en seguridad. Que asegura su carga desde el punto de partida hasta llegar al destino. Monitoreo por medio de drones. En TRANSMODAL utilizamos drones equipados para monitorear su carga." />
	<link rel="icon" href="<?= media(); ?>/images/transmodal.ico" type="image/ico" />
	<meta name="keywords" content="" />
	<meta name="author" content="Gustavo R" />

	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="" />
	<meta property="og:image" content="" />
	<meta property="og:url" content="" />
	<meta property="og:site_name" content="" />
	<meta property="og:description" content="" />
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'> -->

	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/bootstrap.css">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/flexslider.css">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/Assets/css/style.css">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url(); ?>/Assets/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>
	<!-- Header -->
	<div id="fh5co-page">
		<header id="fh5co-header" role="banner">
			<div class="container">
				<div class="header-inner">
					<nav role="navigation" class="navbar navbar-inverse navbar-fixed-top">
						<ul style="padding-left: 100px;">
							<li><a class="" href="<?php echo base_url(); ?>">
									<img src="<?= media(); ?>/images/logoAzul.png" alt="Image, Experiencias." style="width: 300px">
								</a></li>
							<li><a href="<?php echo base_url(); ?>/services">Servicios</a></li>
							<li><a href="<?php echo base_url(); ?>/naval">Naval</a></li>
							<li><a href="<?php echo base_url(); ?>/nosotros">Sobre nosotros</a></li>
							<li><a href="https://www.linkedin.com/company/transmodal-s-c/posts/?feedView=all" target="_BLANK">Noticias</a></li>
							<li><a href="<?php echo base_url(); ?>/sust">Sustentabilidad</a></li>
							<li><a href="https://blueservicessc-sandbox.com/Transmodal/" target="_BLANK">Clientes</a></li>
							<li><a class="btn btn-primary btn-outline" href="<?php echo base_url(); ?>/contacto">Contacto</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>
		<!-- Inicio de contacto -->
		<aside id="fh5co-hero" clsas="js-fullheight">
			<div class="flexslider js-fullheight">
				<ul class="slides">
					<li style="background-image: url(<?= media(); ?>/images/ima/_VM11737.jpg);">
						<div class="overlay-gradient"></div>
						<div class="container">
							<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
								<div class="slider-text-inner">
									<h2 style="color: #0E317A;">Contáctanos</h2>
									<!-- <p class="fh5co-lead">Texto aqui <a href="#" target="_blank"> Texto con hipervinculo</a></p> -->
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</aside>

		<!-- Formulario -->
		<form method="POST">
			<div class="fh5co-contact animate-box">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<h3>Contacto Info.</h3>
							<ul class="contact-info">
								<li><i class="icon-map"></i>Calle Constitución #581, Col. Centro, C.P. 91700, Veracruz, Ver.</li>
								<li><i class="icon-phone"></i>Tel. (229) 775-8800</li>
								<!--<li><i class="icon-envelope"></i><a href="#">info@email.com</a></li>-->
								<li><i class="icon-globe"></i><a href="https://transmodal.com.mx/contacto/" target="_BLANK">transmodal.com.mx</a></li>
							</ul>
						</div>
						<div class="col-md-8 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input class="form-control" placeholder="Nombre/Empresa" type="text" name="name" id="" required="">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input class="form-control" placeholder="mail" type="email" name="email" id="" required="">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input class="form-control" placeholder="Teléfono" type="text" name="phone" id="" required="">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input class="form-control" placeholder="Origen y Destino" type="text" name="datos" id="">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<textarea class="form-control" id="" cols="30" rows="7" placeholder="Mensage" name="message"></textarea>
									</div>
								</div>
								<div class="col-md-12">
									<div class="clear"></div>
									<div class="form-group">
										<button type="submit" class="btn btn-primary">ENVIAR </button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

		<!-- Mapa-->
		<!-- Mapa al 100% -->
		<div class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3767.8893519801013!2d-96.14672508460194!3d19.200034553050216!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85c346a39f66e143%3A0x11057f0b51fe2b69!2sTransmodal!5e0!3m2!1ses-419!2smx!4v1641602451268!5m2!1ses-419!2smx" width="100%" height="200" style="border:1;" allowfullscreen="" loading="lazy"></iframe>
		</div>
		<div class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1881.3153524050226!2d-99.1782947!3d19.4283558!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1ff4d743c0001%3A0x16e3134eb34bd28e!2sTransmodal!5e0!3m2!1ses-419!2smx!4v1644355487463!5m2!1ses-419!2smx" width="100%" height="200" style="border:1;" allowfullscreen="" loading="lazy"></iframe>
		</div>

		<!--<div class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3767.8893519800367!2d-96.14672508582916!3d19.200034553053033!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85c346a39f66e143%3A0x11057f0b51fe2b69!2sTransmodal!5e0!3m2!1ses-419!2smx!4v1644354932005!5m2!1ses-419!2smx" align="right" width="842" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1118.6368010826873!2d-99.1782947290495!3d19.428355834744576!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1ff4d743c0001%3A0x16e3134eb34bd28e!2sTransmodal!5e0!3m2!1ses-419!2smx!4v1644354816170!5m2!1ses-419!2smx" width="842" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
		</div>-->

		<!-- footer -->
		<footer id="fh5co-footer" role="contentinfo">
			<!-- Nuestros servicios -->
			<div class="container">
				<div class="col-md-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
					<h3>Nuestros Servicios</h3>
					<ul class="float">
						<!--  <li><a>Seguridad &amp; Visualizacion</a></li>-->
						<li><a href="<?php echo base_url(); ?>/contacto">Marítimo</a></li>
						<li><a href="<?php echo base_url(); ?>/contacto">Ferrocarril</a></li>
						<li><a href="<?php echo base_url(); ?>/contacto">Camión</a></li>
						<li><a href="<?php echo base_url(); ?>/contacto">Almacén</a></li>
						<li><a href="<?php echo base_url(); ?>/contacto">Seguros</a></li>
						<li><a href="<?php echo base_url(); ?>/contacto">Servicio Personalizado</a></li>
						<li><a href="https://speechnotes.co/es/" target="_BLANK">Convertidor de audio</a></li>
						<li><a href="https://blueservicessc-sandbox.com/Transmodal/" target="_BLANK">Acceso a clientes</a></li>
						<li><a href="https://transmodal.com.mx/aviso-de-privacidad/" target="_BLANK">Aviso de Privacidad</a></li>
					</ul>
				</div>

				<!-- Siguenos -->
				<div class="col-md-4 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0 text-center">
					<h3>Síguenos</h3>
					<ul class="fh5co-social">
						<li><a href="https://twitter.com/TransmodalMx" target="_BLANK"><i class="icon-twitter"></i></a></li>
						<li><a></a></li>
						<li><a href="https://www.facebook.com/Transmodal.sc" target="_BLANK"><i class="icon-facebook"></i></a></li>
						<li><a></a></li>
						<li><a href="https://www.instagram.com/Transmodal_/" target="_BLANK"><i class="icon-instagram"></i></a></li>
						<li><a></a></li>
						<li><a href="https://www.linkedin.com/company/transmodal-s-c/mycompany/" target="_BLANK"><i class="icon-linkedin"></i></a></li>
					</ul>
				</div>
				<!-- sobre nosotros -->
				<!--<div class="container">
          <div class="col-md-4 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
            <h3 align="center">Sobre Nosotros</h3>
            <p align="justify">Somos una empresa 100% mexicana de transportación logística nacional e internacional. Fundada en el año 2001, Transmodal S.C. nace de una visión enfocada en brindar y desarrollar la transportación ferroviaria siendo esta la estrategia principal para un nuevo enfoque logístico.</p>
          </div>-->

				<!-- Navegación -->
				<div class="col-md-2 col-md-push-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
					<h3>Navegación</h3>
					<ul>
						<li><a href="<?php echo base_url(); ?>/services">Servicios</a></li>
						<li><a href="#">Naval</a></li>
						<li><a href="<?php echo base_url(); ?>/nosotros">Sobre nosotros</a></li>
						<li><a href="https://www.linkedin.com/company/transmodal-s-c/posts/?feedView=all" target="_BLANK">Noticias</a></li>
						<li><a href="#">Sustentabilidad</a></li>
						<li><a href="https://blueservicessc-sandbox.com/Transmodal/" target="_BLANK">Acceso clientes</a></li>
						<li><a class="btn btn-primary btn-outline" href="<?php echo base_url(); ?>/contacto">Contacto</a></li>
					</ul>
				</div>


				<!-- Derechos de autor -->
				<div class="col-md-12 fh5co-copyright text-center">
					<p>&copy; 2021 Transmodal S.C. All Rights Reserved. <span>Designed by Transmodal TI 2021</span></p>
				</div>


			</div>
	</div>
	</div>
	</footer>


	<!-- jQuery -->
	<script src="<?= media(); ?>/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?= media(); ?>/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?= media(); ?>/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?= media(); ?>/js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="<?= media(); ?>/js/jquery.flexslider-min.js"></script>
	<!-- Google Map -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
	<script src="<?= media(); ?>/js/google_map.js"></script>

	<!-- MAIN JS -->
	<script src="<?= media(); ?>/js/main.js"></script>

</body>

</html>

<?php
if ($_POST) {
	$name = $_POST['name'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$datos = $_POST['datos'];
	$message = $_POST['message'];

	$message = "Este mensaje es enviado por: " . $name . " \r\n";
	$message .= "E-mail de contacto ingresado: " . $email . " \r\n";
	$message .= "Telefono de contacto: " . $phone . " \r\n";
	$message .= "Datos de ruta: " . $datos . " \r\n";
	$message .= "Mensaje: " . $_POST['message'] . " \r\n";
	$message .= "Enviado el: " . date('d/m/Y', time());

	$para = 'rtello@transmodal.com.mx'; // Correo de contacto "rtello"
	$asunto = 'Cotizacion por pagina web.';
	$header = "Enviado desde pagina web.";


	$send = mail($para, $asunto, $message, $header);
	if ($send) {
		echo "<script>alert('Correo enviado exitosamente')</script>";
	} else {
		echo "<script>alert('No es posible enviar el correo')</script>";
	}
}
?>