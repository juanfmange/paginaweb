<?php

   //define("BASE_URL", "http://localhost/principal/");
   //const BASE_URL = "http://localhost/Inicio";
   const BASE_URL = "https://marketino.digital";

   

   //Zona horaria
   date_default_timezone_set('America/Mexico_City');

   //Datos de conexion de Base de Datos
   const DB_HOST = "localhost";
   const DB_NAME = "db_dashboard";
   const DB_USER = "root";
   const DB_PASSWORD = "";
   const DB_CHARSET = "charset=utf8";

   //Deliminadores decimal y millar Ej. 24,1989.00
   const SPD = ".";
   const SPM = ",";

   //Simbolo de moneda
   const SMONEY = "$";
?>