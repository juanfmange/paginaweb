<?php 

   class Naval extends Controllers{
   	public function __construct()
   	{
        parent::__construct();

   	}
   	public function naval()
   	{
      $data['page_id'] = 4;
      $data['page_tag'] = "Naval - Naval";
      $data['page_title'] = "Naval - Naval";
      $data['page_name'] = "naval";
   		$this->views->getView($this, "naval",$data);
   	}

  }
