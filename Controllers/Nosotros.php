<?php 

   class Nosotros extends Controllers{
   	public function __construct()
   	{
        parent::__construct();

   	}
   	public function nosotros()
   	{
      $data['page_id'] = 4;
      $data['page_tag'] = "Nosotros - Nosotros";
      $data['page_title'] = "Nosotros - Nostros";
      $data['page_name'] = "nosotros";
   		$this->views->getView($this,"nosotros",$data);
   	}

  }

 ?>